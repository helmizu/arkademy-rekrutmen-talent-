# Readme

## Content

Code for solve problem from number 1 to 7. This problem is solved by javascript

## How To Run

Make sure you have installed Node.js

### 1. Convert JSON

1. open directory to /1
2. execute on cmd `node ./1.js`

### 2. Username Validation

1. open directory to /2
2. execute on cmd `node ./2.js`

### 3. Segitiga

1. open directory to /3
2. execute on cmd `node ./3.js`

### 4. Count Handshake

1. open directory to /4
2. execute on cmd `node ./4.js`

### 5. Count Char in Word

1. open directory to /5
2. execute on cmd `node ./5.js`

### 6. Make Query for Get Some Data

![SS Query](6/Screenshot(11).png)

1. open directory to /6
2. import database from the existing sql file
3. open mysql from cmd
4. use database was import
5. execute this query `select product_categories.id, product_categories.name, count(product.category_id) as jumlah_product from product_categories join product on product.category_id = product_categories.id group by product_categories.id`
6. or you can execute that query with existing js file
7. first, open cmd and install node_modules with `npm install`
8. next execute `node ./6.js` and data will served as json

### 7. Make simple web application to Display data from query in number 6

![SS Web](7/ss/ss1.png)

#### This web app make by

* backend : node.js with express
* frontend : html using bootstrap and jquery

#### how to operate

1. open directory to /7
2. install nod_modules for backend with execute `npm install --prefix backend`
3. run backend with execute `npm start --prefix backend`, server will run on port 3000
4. open file index.html and data will displayed