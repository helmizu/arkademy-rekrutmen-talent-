const isValidUsername = (username = '') => {
    let error = 0
    if (username.length !== 8) return {msg : "username harus 8 karakter"}

    if (username.substr(0, 5).match(/[a-z]/g).length === 5) {
        console.log('huruf kecil pada karakter 1-5 benar')
    } else {
        error++
        console.error('Error : karakter 1-5 harus huruf kecil');
    }

    if (username.charAt(5).match(/[_.]/g).length === 1) {
        console.log('(_) atau (.) pada karakter 6 benar')
    } else {
        error++
        console.error('Error : Karakter 6 harus (_) atau (.)');
    }

    if (username.substr(6, 2).match(/[A-Z]/g).length === 2) {
        console.log('huruf besar pada karakter 7-8 benar')
    } else {
        error++
        console.error('Error : karakter 1-5 harus huruf besar');
    }

    if (error === 0) {
        return {error, msg : 'username valid'}
    } else {
        return {error, msg : 'username tidak valid'}
    }
}

isValidUsername("lkjhg.AA")
console.log(isValidUsername("qwert_QQ"))