const myCountChar = (word = '', substr = '') => {
    var regexp = new RegExp(substr, 'g')
    var count = (word.toLowerCase().match(regexp) || []).length
    return console.log(count)
}

myCountChar('bootcamp', 'o')
myCountChar('arkademy', 'k')