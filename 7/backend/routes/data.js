const express = require('express');
const router = express.Router();
const mysql = require('mysql');

const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "arkademy_rekrutmen_talent"
})

db.connect((err) => {
  if (err) throw err
  console.log('Database Connected')
})

router.get('/detail_categories', (req, res) => {
  const sql = "select product_categories.id, product_categories.name, count(product.category_id) as jumlah_product from product_categories join product on product.category_id = product_categories.id group by product_categories.id"
  db.query(sql, (err, result) => {
    if (err) throw err
    res.json(result);
  })
})
router.get('/product', (req, res) => {
  const sql = "select product.id, product.name, product_categories.name, product.created_date from product join product_categories on product.category_id = product_categories.id"
  db.query(sql, (err, result) => {
    if (err) throw err
    res.json(result);
  })
})
router.get('/categories', (req, res) => {
  const sql = "select product_categories.id, product_categories.name, product_categories.created_date from product_categories"
  db.query(sql, (err, result) => {
    if (err) throw err
    res.json(result);
  })
})

module.exports = router;
