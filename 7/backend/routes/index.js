const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.json({msg : "Get data on /data/categories, /data/product, /data/detail_categories"})
})

module.exports = router;
