const segitigaAsli = (deret = 0) => {
    if (!(deret > 0 && deret <=10)) return console.error("deret harus lebih dari 0 dan tidak boleh lebih dari 10")
    let segitiga = ''
    for (let index = 1; index <= deret; index++) {
        segitiga += `${index},\t`
        console.log(segitiga)
    }
    return
}

segitigaAsli(7)