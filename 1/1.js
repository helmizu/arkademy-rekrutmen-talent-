var itemId = 12341822
var itemName = "basic t-shirt"
var price = 70000
var availableColorAndSize = [
    {
        color : "red",
        size : "S, M, L" 
    },
    {
        color : "black",
        size : "M, L"
    }
]
var freeShiping = false

const convertJson = (itemId = 0, itemName = '', price = 0, availableColorAndSize = [{color: '', size: ''}], freeShiping = false) => {
    return {
        itemId,
        itemName,
        price,
        availableColorAndSize,
        freeShiping
    }
}

console.log(convertJson(itemId, itemName, price, availableColorAndSize, freeShiping))