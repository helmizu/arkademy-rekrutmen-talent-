const handshakeCounter = (people = 0) => {
    let handshake = 0;
    if (!(people > 1)) return {people, handshake, msg : "orang harus lebih dari 1"}

    for (let index = people; index > 1; index--) {
        handshake += (index - 1)
    }

    return handshake
}

console.log(handshakeCounter(3))
console.log(handshakeCounter(6))